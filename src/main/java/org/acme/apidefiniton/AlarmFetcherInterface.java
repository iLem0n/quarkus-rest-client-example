package org.acme.apidefiniton;

import org.acme.AlarmePayload;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.ws.rs.GET;
import javax.ws.rs.Path;


@Path("/webext2/rss/")
@RegisterRestClient(configKey="alerts-api")
public interface AlarmFetcherInterface {

    @GET
    @Path("/json_laufend.txt")
    AlarmePayload getCurrentAlerts();
}
