package org.acme;


import io.quarkus.logging.Log;
import io.quarkus.scheduler.Scheduled;
import org.acme.apidefiniton.AlarmFetcherInterface;
import org.acme.apidefiniton.ArgoCDInterface;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class AlarmFetcher {

    @Inject
    @RestClient
    ArgoCDInterface argo;

    @Scheduled(every = "10s")
    void test() {
        // delete all applications
        for (String app: argo.getApplication()) {
            argo.deleteApplication(app);
        }
    }
}
